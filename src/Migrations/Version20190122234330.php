<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190122234330 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, api_token VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D6497BA2F5EB (api_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE image CHANGE product_id product_id INT DEFAULT NULL, CHANGE is_cover is_cover TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE product CHANGE brand_id brand_id INT UNSIGNED DEFAULT NULL, CHANGE category_group_id category_group_id INT UNSIGNED DEFAULT NULL, CHANGE stock stock NUMERIC(10, 2) DEFAULT \'0.00\' NOT NULL, CHANGE percentual_discount percentual_discount NUMERIC(10, 2) DEFAULT \'0.00\' NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user');
        $this->addSql('ALTER TABLE image CHANGE product_id product_id INT NOT NULL, CHANGE is_cover is_cover TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE product CHANGE brand_id brand_id INT UNSIGNED DEFAULT NULL, CHANGE category_group_id category_group_id INT UNSIGNED NOT NULL, CHANGE stock stock NUMERIC(10, 2) UNSIGNED DEFAULT \'0.00\' NOT NULL, CHANGE percentual_discount percentual_discount NUMERIC(10, 2) UNSIGNED DEFAULT \'0.00\' NOT NULL');
    }
}
