<?php

namespace App\Controller;

use App\Entity\Brand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class BrandController extends AbstractController
{

    private function serializer()
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        return new Serializer($normalizers, $encoders);
    }

    /**
     * @Route("/brand/{id}", name="brand_show", methods={"GET"})
     */
    public function show(Brand $brand): Response
    {
        $jsonContent = $this->serializer()->serialize($brand, 'json');

        return $this->json($jsonContent, 200, ["Access-Control-Allow-Origin"=>"*"]);
    }

    /**
     * @Route("/brand/{id}", name="brand_edit", methods={"PUT"})
     */
    public function edit(Request $request, Brand $brand): Response
    {

        $brand = $this->serializer()->deserialize(json_encode($request->request->all()), Brand::class, 'json', ['object_to_populate' => $brand]);

        $this->getDoctrine()->getManager()->flush();

        return $this->json(['success' => true, 'id' => $brand->getId()], 200, ["Access-Control-Allow-Origin"=>"*"]);
    }

    /**
     * @Route("/brand/{id}", name="brand_options", methods={"OPTIONS"})
     */
    public function options(): Response
    {
        $response = $this->json(['success' => true], 200, [
            "Access-Control-Allow-Origin"=>"*",
            "Access-Control-Allow-Methods" => "POST, GET, PUT, DELETE",
            "Access-Control-Allow-Headers"=> "*"
            ]);

        return $response;
    }

    /**
     * @Route("/brand/{id}", name="brand_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Brand $brand): Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($brand);
        $entityManager->flush();

        return $this->json(['success' => true], 200, ["Access-Control-Allow-Origin"=>"*"]);
    }

    /**
     * @Route("/brand", name="brand_index", methods={"GET"})
     */
    public function index(): Response
    {
        $brands = $this->getDoctrine()
            ->getRepository(Brand::class)
            ->findAll();

        $brandsArray = [];
        $normalizer = new GetSetMethodNormalizer();

        foreach ($brands as $brand){
            $brandsArray[] = $normalizer->normalize($brand);
        }

        $normalizer = new GetSetMethodNormalizer();

        return $this->json($brandsArray, 200, ["Access-Control-Allow-Origin"=>"*"]);
    }

    /**
     * @Route("/brand", name="brand_new", methods={"POST"})
     */
    public function new(Request $request): Response
    {

        $brand = $this->serializer()->deserialize(json_encode($request->request->all()), Brand::class, 'json');

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($brand);
        $entityManager->flush();

        if (!$brand->getId()) {
            return $this->json(['success' => false, 'id' => 'Nao pode criar uma marca']);
        }

        return $this->json(['success' => true, 'id' => $brand->getId()], 200, ["Access-Control-Allow-Origin"=>"*"]);

    }

}
