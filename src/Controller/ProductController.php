<?php

namespace App\Controller;

use App\Entity\Brand;
use App\Entity\Product;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ProductController extends AbstractController
{

    private function serializer()
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        return new Serializer($normalizers, $encoders);
    }

    /**
     * @Route("/product/{id}", name="product_show", methods={"GET"})
     */
    public function show(Product $product): Response
    {
        $jsonContent = $this->serializer()->serialize($product, 'json');

        return $this->json($jsonContent, 200, ["Access-Control-Allow-Origin"=>"*"]);
    }

    /**
     * @Route("/product/{id}", name="product_edit", methods={"PUT"})
     */
    public function edit(Request $request, Product $product): Response
    {

        $data = $request->request->all();
        $entityManager = $this->getDoctrine()->getManager();

        $product->setName($data['name']);
        $product->setPrice($data['price']);
        $product->setStock($data['stock']);


        $brand = $entityManager->getRepository(Brand::class)->find($data['brand']);
        $product->setBrand($brand);

        $entityManager->persist($product);
        $entityManager->flush();

        return $this->json(['success' => true, 'id' => $product->getId()], 200, ["Access-Control-Allow-Origin"=>"*"]);
    }

    /**
     * @Route("/product/{id}", name="product_options", methods={"OPTIONS"})
     */
    public function options(): Response
    {
        $response = $this->json(['success' => true], 200, [
            "Access-Control-Allow-Origin"=>"*",
            "Access-Control-Allow-Methods" => "POST, GET, PUT, DELETE",
            "Access-Control-Allow-Headers"=> "*"
            ]);

        return $response;
    }

    /**
     * @Route("/product/{id}", name="product_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Product $product): Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($product);
        $entityManager->flush();

        return $this->json(['success' => true], 200, ["Access-Control-Allow-Origin"=>"*"]);
    }

    /**
     * @Route("/product", name="product_index", methods={"GET"})
     */
    public function index(): Response
    {
        $data = $this->getDoctrine()->getManager()->createQuery(
            'SELECT p.id, identity(p.brand) as  brand, p.name, p.price, p.stock
            FROM App\Entity\Product p
            ORDER BY p.price ASC'
        )
            ->getResult(Query::HYDRATE_ARRAY);

        return $this->json($data, 200, ["Access-Control-Allow-Origin"=>"*"]);
    }

    /**
     * @Route("/product", name="product_new", methods={"POST"})
     */
    public function new(Request $request): Response
    {

        $data = $request->request->all();

        $product = new Product();
        $product->setName($data['name']);
        $product->setPrice($data['price']);
        $product->setStock($data['stock']);

        $entityManager = $this->getDoctrine()->getManager();

        $brand = $entityManager->getRepository(Brand::class)->find($data['brand']);
        $product->setBrand($brand);

        $entityManager->persist($product);
        $entityManager->flush();

        if (!$product->getId()) {
            return $this->json(['success' => false, 'id' => 'Nao pode criar uma marca']);
        }

        return $this->json(['success' => true, 'id' => $product->getId()], 200, ["Access-Control-Allow-Origin"=>"*"]);

    }

}
