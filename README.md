# Configuração

Crie um banco de dados chamado mentores

Importe o sql que está em database/db.sql

Execute o composer install
 
```php7.1 composer.phar install```

Adicione uma entrada no arquivo hosts com o endereço dev.mentores apontando para o servidor

```127.0.0.1 dev.mentores```

É necessário que esta aplicação responta na porta 80